<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnotacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anotacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('idagenda');
            $table->string('title_agenda');
            $table->text('anotation');
            $table->unsignedInteger('iduser');
            $table->foreign('idagenda')
                  ->references('id')
                  ->on('agendas');
            $table->foreign('iduser')
                  ->references('id')
                  ->on('users');                  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anotacoes');
    }
}
