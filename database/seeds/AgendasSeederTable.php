<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AgendasSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('agendas')->insert([
        	[
        		'title' => 'Ambientare',
        		'color' => '#000',
        		'start' => '2018-11-26 10:00:00',
        		'end' => '2018-11-26 10:00:00',
                'iduser' => 1,
        		'created_at' => now(),
        		'updated_at' => now()
        	]
        ]);
    }
}
