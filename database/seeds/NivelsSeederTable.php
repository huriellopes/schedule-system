<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NivelsSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	DB::table('nivels')->insert([
     		[
     			'nivel' => 'A',
     			'desc' => 'Administrador',
     			'created_at' => now(),
     			'updated_at' => now(),
     		],
     		[
     			'nivel' => 'U',
     			'desc' => 'Usuário Comum',
     			'created_at' => now(),
     			'updated_at' => now(),
     		]
     	]);   
    }
}
