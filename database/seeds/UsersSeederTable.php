<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' 		=> 'Admin',
        	'email' 	=>	'admin@admin.com',
        	'password' 	=> bcrypt('123456'),
        	'idnivel'	=> '1',
        	'remember_token' => str_random(10),
        	'created_at' => now(),
        	'updated_at' => now(),
        ]);
    }
}
