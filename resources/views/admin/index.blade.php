@extends('adminlte::page')

@section('title', 'Agenda')

@section('fullcalendar-css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
@stop

@section('content')
    <div class="container-fluid">
        <div class="row mt-2">
            <div class="col-md-12 col-sm-12">
                <h2>Bem Vindo <strong>{{ auth()->user()->name }}</strong></h2>
            </div>
        </div>
        <div class="row my-2" style="width: 75%;">
            {!! $chartjs->render() !!}
        </div>
        <div class="row mt-2">
            <div class="col-md-6 col-sm-12">
                <h2>Calendário de Reuniões</h2>
            </div>
            <div class="col-md-6 col-sm-12">
                <a href="{{ url('/admin/agenda/newReuniao') }}" class="btn btn-primary botaoNew">Nova Reunião</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                {!! $calendar->calendar() !!}
            </div>
        </div>
    </div>
@stop

@section('fullcalendar-script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/locale/pt-br.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    @toastr_render
	{!! $calendar->script() !!}
    <script>
        var date = new Date();
        $chart->optionsRaw([
            'legend' => [
                'display' => true,
                'labels' => [
                    'fontColor' => '#000'
                ]
            ],
            'scales' => [
                'xAxes' => [
                    [
                        'stacked' => true,
                        'gridLines' => [
                            'display' => true
                        ]
                    ]
                ]
            ]
        ]);
    </script>
@stop