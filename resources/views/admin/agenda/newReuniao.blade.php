@extends('adminlte::page')

@section('title', 'Agenda')

@section('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/jquery.datetimepicker.min.css" />
@stop

@section('content_header')
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <h1>Nova Reunião</h1>
        </div>
      </div>
    </div>
@stop

@section('content')
  <div class="container-fluid">
    <div class="row mt-5">
      <div class="col-md-12 col-sm-12">
        <form action="{{ url('/admin/agenda/saveReuniao') }}" method="POST" autocomplete="off">
          <div class="form-row">
            <div class="col-md-4 col-sm-12 form-group">
              {!! csrf_field() !!}
              <label>Título</label>
              <input type="text" name="title" class="form-control" autofocus required autocomplete="off" />
            </div>
            <div class="col-md-4 col-sm-12 form-group">
              <label>Inicio em </label>
              <input type="text" name="start" id="startText" class="form-control date" required autocomplete="off" />
            </div>
            <div class="col-md-4 col-sm-12 form-group">
              <label>Final em</label>
              <input type="text" name="end" id="endText" class="form-control date" required autocomplete="off" />
            </div>
          </div>
          <div class="form-row">
            <div class="col-md-12 col-sm-12 form-group">
              <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@stop

@section('js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.min.js"></script>
  <script src="http://cdn.craig.is/js/rainbow-custom.min.js"></script>
  <script>
    $(function() {
      $.datetimepicker.setLocale('pt-BR');
      $('#startText').datetimepicker({
        format: 'd/m/Y H:i'
      });
      $('#endText').datetimepicker({
        format: 'd/m/Y H:i',
      });
    });
  </script>
@stop