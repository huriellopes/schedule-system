@extends('adminlte::page')

@section('title', 'Agenda')

@section('css')
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
@stop

@section('content_header')
<div class="container-fluid">
  <div class="row align-items-center">
    <div class="col-6 col-md-6 col-sm-12">
      <h1>Lista de Anotações</h1>
    </div>
    <div class="col-6 col-md-6 col-sm-12">
      <a href="{{ url('/admin/agenda/newAnotation') }}" class="btn btn-primary botaoNew">Nova Anotação</a>
    </div>
  </div>
</div>
@stop

@section('content')
   <div class="container-fluid">
   	<div class="row mt-5">
   		<div class="col-md-12 col-sm-12">
   			<table class="table table-hover table-striped" id="example2">
          <thead>
            <tr>
              <th>Reunião</th>
              <th>Título da Reunião</th>
              <th>Criado por</th>
              <th>Criado em</th>
              <th>Atualizado em</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>
            @foreach($anotations as $anotation)
              <tr>
                <td>{{ $anotation->title }}</td>
                <td>{{ $anotation->title_agenda }}</td>
                <td>{{ $anotation->name }}</td>
                <td>{{ date('d/m/Y H:i:s',strtotime($anotation->created_at)) }}</td>
                <td>{{ $anotation->updated_at==date('d/m/Y H:i:s',strtotime($anotation->updated_at))?$anotation->updated_at:'Não Atualizado' }}</td>
                <td>
                  <a href="{{ url('/admin/agenda/viewAnotation/'.$anotation->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                </td>
              </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>Reunião</th>
              <th>Título da Reunião</th>
              <th>Criado por</th>
              <th>Criado em</th>
              <th>Atualizado em</th>
              <th>Ações</th>
            </tr>
          </tfoot>
        </table>
   		</div>
   	</div>
   </div>
@stop

@section('js')
  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  @toastr_render
  
  <script type="text/javascript">
    $(function() {
      $("#example1").DataTable();
      $('#example2').DataTable({
        "destroy": true,
        "pageLength": 3,
        "processing": true, //Exibe a informação de que o conteúdo está sendo processado
        "serverSide": false, //Define se a busca e a paginação serão a nivel server-side ou client-side
        "dom": 'Bfrtip',
        "aoColumnDefs": [
            {"bSortable": false, "aTargets": [0]}
        ],
        "aaSorting": [[0, 'asc']],
        "oLanguage": {
          "sProcessing": "Aguarde enquanto os dados são carregados ...",
          "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
          "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
          "sInfo": "Exibindo de _START_ até _END_ de _TOTAL_ registros",
          "sInfoFiltered": "",
          "sSearch": "Buscar: ",
          "oPaginate": {
            "sFirst": "Início",
            "sPrevious": "Anterior",
            "sNext": "Próximo",
            "sLast": "Último"
          }
        }
      });
    });
  </script>
@stop