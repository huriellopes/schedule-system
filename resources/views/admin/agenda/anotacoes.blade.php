@extends('adminlte::page')

@section('title', 'Agenda')

@section('trix-css')
  <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/trix.css') }}" />
@stop

@section('content_header')
    <div class="container-fluid">
    	<div class="row">
    		<div class="col-md-12 col-sm-12">
    			<h1>Anotações de Reuniões</h1>
    		</div>
    	</div>
    </div>
@stop

@section('content')
   <div class="container-fluid">
   	<div class="row">
   		<div class="col-md-12 col-sm-12">
   			<form action="{{ url('/admin/agenda/saveAnotation') }}" method="POST">
   				<div class="form-row">
   					<div class="col-md-5 col-sm-12 form-group">
   						<label>Reunião</label>
   						<select name="idagenda" class="form-control">
   							<option value selected="true" disabled="true">Selecione a Reunião</option>
   							@foreach($all as $agenda)
   								<option value="{{ $agenda->id }}">{{ $agenda->title }} - {{ date('d/m/Y H:i:s',strtotime($agenda->start)) }}</option>
   							@endforeach
   						</select>
   					</div>
   					<div class="col-md-7 col-sm-12 form-group">
   						{!! csrf_field() !!}
   						<label>Título da Reunião</label>
   						<input type="text" name="title_agenda" class="form-control" autofocus />
   					</div>
   				</div>
   				<div class="form-row">
   					<div class="col-md-12 col-sm-12 form-group">
   						<label>Anotações</label>
              <input id="txtAnotation" type="hidden" name="anotation" required />
              <trix-editor input="txtAnotation"></trix-editor>
   					</div>
   				</div>
   				<div class="form-row">
   					<div class="col-md-12 col-sm-12 form-group">
   						<button type="submit" class="btn btn-primary">Salvar</button>
   					</div>
   				</div>
   			</form>
   		</div>
   	</div>
   </div>
@stop

@section('trix-js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.0.0/trix.js"></script>
  <script>
    document.addEventListener('trix-attachment-add', function () {
      const attachment = event.attachment;

      if (!attachment.file) {
        return;
      }
      const form = new FormData();
      form.append('file', attachment.file);

      $.ajax({
        url: '/admin/upload/image',
        method: 'POST',
        data: form,
        contentType: false,
        processData: false,
        xhr: function () {
          const xhr = $.ajaxSettings.xhr();
          xhr.upload.addEventListener('progress', function (e) {
            let progress = e.loaded / e.total * 100;
            attachment.setUploadProgress(progress);
          });

          return xhr;
        }
      }).done(function (response) {
            console.log(response);
            attachment.setAttributes({
            url: response,
            href: response
          });
        }).fail(function () {
          console.log('deu Errado');
        });
    });
  </script>
@stop