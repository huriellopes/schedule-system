@extends('adminlte::page')

@section('title', 'Agenda')

@section('content_header')
  <div class="container-fluid">
   	<div class="row">
   		<div class="col-md-12 col-sm-12">
   			<h1>Visualização de Anotação</h1>
   		</div>
  	</div>
  </div>
@stop

@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8 col-sm-12">
        <div class="card">
          @foreach($anotation as $especific)
            <div class="card-body">
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><strong>ID - </strong>{{ $especific->id }}</li>
                <li class="list-group-item"><strong>Reunião - </strong>{{ $especific->title }}</li>
                <li class="list-group-item"><strong>Relação da Reunião - </strong> {{ $especific->anotation }}</li>
                <li class="list-group-item"><strong>Criado por - </strong>{{ $especific->name }}</li>
                <li class="list-group-item"><strong>Criado em - </strong>{{ date('d/m/Y H:i:s', strtotime($especific->created_at)) }}</li>                
                <li class="list-group-item"><strong>Atualizado em - </strong>{{ $especific->updated_at==date('d/m/Y H:i:s',strtotime($especific->updated_at))?$especific->updated_at:'Não Atualizado' }}</li>
              </ul>
            </div>
            <div class="card-footer">
              <a href="{{ url('/admin/agenda/editAnotation/'.$especific->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i> Editar</a>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
@stop