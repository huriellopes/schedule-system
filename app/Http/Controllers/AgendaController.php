<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Agenda;
use App\Models\Anotacao;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

class AgendaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	return view('vendor.adminlte.login');
    }

    public function admin()
    {
    	$events = [];
        $agenda = new Agenda();
        $data = $agenda->getAgenda();
        if($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    $value->title,
                    true,
                    new \DateTime($value->start),
                    new \DateTime($value->end.' +1 day'),
                    null,
                    // Add color and link on event
                    [
                        'allDay' => true,
                        'color' => $value->color,
                        'url' => url('/admin/agenda/editReuniao/'.$value->id),
                        'editable' => false,
                        
                    ]
                );
            }
        }
        $calendar = Calendar::addEvents($events);

        $agendas = new Agenda();
        $chartjs = app()->chartjs
                ->name('barChartTest')
                ->type('bar')
                ->size(['width' => 400, 'height' => 200])
                ->labels(['Realizados','Pendentes'])
                ->datasets([
                    [
                        "label" => "Realizados",
                        'backgroundColor' => ['rgba(0,132,15, 0.9)', 'rgba(9,163,27, 0.2)'],
                        'data' => [$agendas->countRealizadas()]
                    ],
                    [
                        "label" => "Pendentes",
                        'backgroundColor' => ['rgba(142, 0, 0, 0.9)', 'rgba(132, 29, 29, 0.3)'],
                        'data' => [$agendas->countPendentes()]
                    ]
                ])
                ->options([]);

        return view('admin.index', compact('calendar','chartjs'));
    }

    public function newReuniao()
    {
        return view('admin.agenda.newReuniao');
    }

    public function saveReuniao(Request $request)
    {
        $agenda = new Agenda();
        $confirm = $agenda->saveAgenda($request->title, $request->start, $request->end);
        if ($confirm):
            toastr()->success('Reunião Cadastrado com Sucesso!');
            return redirect()->to('/admin/');
        else:
            toastr()->danger('Ops.. Deu algum problema, procure o Administrador!');
            return redirect()->to('/admin/');
        endif;
    }

    public function editReuniao($id)
    {
        $agenda = new Agenda();
        $find = $agenda->findReuniao($id);
        return view('admin.agenda.editReuniao', compact('find'));
    }

    public function upReuniao(Request $request)
    {
        $agenda = new Agenda();
        $confirm = $agenda->upAgenda($request->title, $request->start, $request->end, $request->id);
        if ($confirm):
            toastr()->success('Reunião Atualizada com Sucesso!');
            return redirect()->to('/admin/');
        else:
            toastr()->danger('Ops.. Deu algum problema, procure o Administrador!');
            return redirect()->to('/admin/');
        endif;
    }

    public function listAnotacao()
    {
        $anotacoes = new Anotacao();
        $anotations = $anotacoes->getAnotacoes();
        return view('admin.agenda.listAnotacao', compact('anotations'));
    }

    public function anotacoes()
    {
        $agenda = new Agenda();
        $all = $agenda->getAgenda();
    	return view('admin.agenda.anotacoes', compact('all'));
    }

    public function saveAnotation(Request $request)
    {
        $agenda = new Anotacao();
        $confirm = $agenda->saveAnotations($request->idagenda, $request->title_agenda, $request->anotation);
        if ($confirm):
            toastr()->success('Anotação Cadastrada com Sucesso!');
            return redirect()->to('/admin/agenda/anotacao');
        else:
            toastr()->danger('Ops.. Deu algum problema, procure o Administrador!');
            return redirect()->to('/admin/agenda/anotacao');
        endif;
    }

    public function viewAnotation($id)
    {
        $agenda = new Anotacao();
        $anotation = $agenda->findAnotacoes($id);
        return view('admin.agenda.viewAnotation', compact('anotation'));
    }

    public function editAnotation($id)
    {
        $agenda = new Anotacao();
        $agendas = new Agenda();
        $anotation = $agenda->findAnotacoes($id);
        $all = $agendas->getAgenda();
        return view('admin.agenda.editAnotation', compact('anotation','all'));
    }

    public function EditSaveAnotation(Request $request)
    {
        $anotation = new Anotacao();
        $confirm = $anotation->EditSaveAnotation($request->idagenda, $request->title_agenda, $request->anotation, $request->id);
        if ($confirm):
            toastr()->success('Anotação Atualizada com Sucesso!');
            return redirect()->to('/admin/agenda/anotacao');
        else:
            toastr()->danger('Ops.. Deu algum problema, procure o Administrador!');
            return redirect()->to('/admin/agenda/anotacao');
        endif;
    }

    public function logout()
    {
        auth()->logout();

        return redirect(\Request::is('admin*') ? '/admin' : '/');
    }
}
