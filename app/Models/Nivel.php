<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Nivel extends Model
{
    public function getNivel()
    {
    	return DB::table('nivels')
    				->select('*')
    				->get();
    }
}
