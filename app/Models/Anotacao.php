<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Anotacao extends Model
{
    public function getAnotacoes()
    {
    	return DB::table('anotacoes')
    				->join('agendas', 'anotacoes.idagenda','=','agendas.id')
                    ->join('users', 'anotacoes.iduser','=','users.id')
    				->select('agendas.title','anotacoes.title_agenda','anotacoes.created_at','anotacoes.updated_at','anotacoes.*','users.*')
    				->get();
    }

    public function updateAgenda($id)
    {
        return DB::table('agendas')
                        ->where('id','=',$id)
                        ->update([
                            'color' => '#00840f',
                            'action' => 'R',
                            'updated_at' => now()
                        ]);
    }

    public function saveAnotations($idagenda,$title,$anotation)
    {
        $this->updateAgenda($idagenda);

    	return DB::table('anotacoes')->insert([
    		'idagenda' => $idagenda,
    		'title_agenda' => $title,
    		'anotation' => $anotation,
            'iduser' => auth()->user()->id,
    		'created_at' => now()
    	]);
    }

    public function findAnotacoes($id)
    {
        return DB::table('anotacoes')
                    ->join('agendas', 'anotacoes.idagenda','=','agendas.id')
                    ->join('users', 'anotacoes.iduser','=','users.id')
                    ->where('anotacoes.id','=',$id)
                    ->select('agendas.title','anotacoes.title_agenda','anotacoes.created_at','anotacoes.updated_at','anotacoes.*','users.*')
                    ->get();
    }

    public function EditSaveAnotation($idagenda,$title,$anotation,$id)
    {
        $this->updateAgenda($idagenda);
        return DB::table('anotacoes')
                    ->where('id','=',$id)
                    ->update([
                        'idagenda' => $idagenda,
                        'title_agenda' => $title,
                        'anotation' => $anotation,
                        'updated_at' => now()
                    ]);
    }
}
