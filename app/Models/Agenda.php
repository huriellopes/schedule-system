<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Agenda extends Model
{
    protected $fillable = ['title','color','start','end'];

    public function getAgenda()
    {
    	return DB::table('agendas')
    			->select('*')
    			->get();
    }

    public function saveAgenda($title,$start,$end)
    {
    	return DB::table('agendas')->insert([
    					'title' => $title,
    					'color' => '#8e0000',
    					'start' => date('Y-m-d H:i:s',strtotime(str_replace('/','-',$start))),
    					'end' => date('Y-m-d H:i:s',strtotime(str_replace('/','-',$end))),
    					'iduser' => auth()->user()->id,
    					'action' => 'P',
    					'created_at' => now()
    				]);
    }

    public function countRealizadas()
    {
    	return DB::table('agendas')
    				->where('action','=','R')
    				->count();
    }

    public function countPendentes()
    {
    	return DB::table('agendas')
    				->where('action','=','P')
    				->count();
    }

    public function findReuniao($id)
    {
    	return DB::table('agendas')
    				->where('id','=',$id)
    				->select('*')
    				->get();
    }

    public function VerificaAction()
    {
    	return DB::table('agendas')
    				->where('action','<>','R')
    				->select('*')
    				->get();
    }

    public function upAgenda($title,$start,$end,$id)
    {
    	$this->VerificaAction();
    	return DB::table('agendas')
    					->where('id','=',$id)
    					->update([
    						'title' => $title,
	    					'color' => '#8e0000',
	    					'start' => date('Y-m-d H:i:s',strtotime(str_replace('/','-',$start))),
	    					'end' => date('Y-m-d H:i:s',strtotime(str_replace('/','-',$end))),
	    					'updated_at' => now()
    					]);
    }
}
