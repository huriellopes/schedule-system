<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AgendaController@index')->name('login');

Route::group(['middleware' => 'auth', 'prefix' => '/admin'], function () {
	Route::get('/', 'AgendaController@admin')->name('admin');
	Route::get('/agenda/anotacao','AgendaController@listAnotacao')->name('anotacao');
	Route::get('/agenda/newReuniao', 'AgendaController@newReuniao')->name('newReuniao');
	Route::post('/agenda/saveReuniao', 'AgendaController@saveReuniao')->name('saveReuniao');
	Route::get('/agenda/editReuniao/{id}', 'AgendaController@editReuniao')->name('editReuniao');
	Route::post('/agenda/{id}/upReuniao', 'AgendaController@upReuniao')->name('upReuniao');
	Route::get('/agenda/newAnotation', 'AgendaController@anotacoes')->name('newAnotation');
	Route::post('/agenda/saveAnotation', 'AgendaController@saveAnotation')->name('saveAnotation');
	Route::get('/agenda/viewAnotation/{id}', 'AgendaController@viewAnotation')->name('viewAnotation');
	Route::get('/agenda/editAnotation/{id}', 'AgendaController@editAnotation')->name('editAnotation');
	Route::post('/agenda/{id}/EditSaveAnotation', 'AgendaController@EditSaveAnotation')->name('EditSaveAnotation');
	Route::post('/logout', 'AgendaController@logout')->name('logout');

});

Auth::routes();
