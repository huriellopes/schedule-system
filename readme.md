# Schedule System

> Sistema desenvolvido com o intuito de facilitar a organização de eventos e reuniões como um todo, sendo que esse sistema tem enfase em reuniões, com gráfico de barra que te dá a estátisca de quantas reuniões foram realizadas e estão pendentes até o exato momento, assim como tem um calendário para mostrar as reuniões marcadas, tendo também um área de anotação das reuniões!

# Requirements

- PHP >= 7.1
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- Json PHP Extension
- MySQL or your preference
- Composer

# O que foi implementado?

	- DataTable
	- Chartjs
	- FullCalendar
	- Toastr
	- AdminLte
	- Trix Editor
	- DateTimePicker

> O que pode ser implementado futuramente?

	> Pode ser desenvolvido futuramente o sistema de envio de e-mail para notificação de reuniões marcadas, Cadastro de Usuários, outra dashboard para usuários com nivel de acesso!

# Como utilizar o sistema?

> Para utilizar, clone ou faça o download do repositório:

	> git clone https://gitlab.com/huriellopes/schedule-system

> Abra o terminal e navegue até a pasta do sistema:

	> composer install
	> npm install

> Antes de rodar, vá no arquivo ".env" e faça as seguintes alterações:

	DB_CONNECTION=mysql
	DB_HOST=127.0.0.1
	DB_PORT=3306
	DB_DATABASE=database
	DB_USERNAME=username
	DB_PASSWORD=secrete

> Em seguida, crie o banco de dados, conforme definiu no arquivo .env

> Depois volte para o terminal, vá ate a pasta do sistema e rode o seguiten comando:

	> php artisan migrate --seed

> Depois de rodar a migrate, rode o comando a seguir, para startar o servidor:

	> php artisan serve

> Vá até o navegador e acesse o seguinte endereço:

	localhost:8000

> Para acessar o dashboard, use as seguintes credenciais:

	email: admin@admin.com.br
	senha: 123456


# Credits

> O Schedule System foi desenvolvido por mim mesmo: Huriel Lopes, peço que caso seja utilizado comercialmente, me deêm os créditos de desenvolvimento!

- Caso queiram tirar dúvidas ou até mesmo fazer parceria de algum projeto:
	
	- E-mail:
	> huriellopes.dev@gmail.com
